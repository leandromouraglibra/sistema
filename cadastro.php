<?php include ('cabecalho.php') ?>

<div class="form-group">
	<h1>Cadastro</h1>
	<form action="adiciona-cadastro.php">
		<div class="row">
			<div class="col-md-6">
				Nome: <input class="form-control" type="text" required="" name="nome" /><br/>
			</div>
			<div class="cod-md-4">
				Unidades:
				<select class="form-control" name="unidade" required>
			
				<option value=""></option>
				<option value="TEVAL">TEVAL</option>
				<option value="CAMPINAS">CAMPINAS</option>
				<option value="RIO">RIO</option>
				<option value="INTERMODAL">INTERMODAL</option>
			
				</select>
			</div>
			<div class="col">
				Pacote / Demanda: <input class="form-control" type="text" name="pacote" required="" /><br/>
			</div>
			<!-- Preço: <input class="form-control" type="number" step='0.01' required="" name="preco" /><br/> -->

			<div class="col-md-12">
				Descrição: <textarea class="form-control" type="text" required="" name="descricao" /></textarea><br/>
			</div>
			<div class="col">
				Data GMUD: <input class="form-control" type="date" name="data_gmud" min="2018-01-01" max="2032-12-31"/><br/>
			</div>
			<div class="col">
				Data Execução QA: <input class="form-control" type="date" name="data_execucao" min="2018-01-01" max="2032-12-31" /><br/>
			</div>
			<div class="col">
				Data Execução PRD: <input class="form-control" type="date" name="data_prd" min="2018-01-01" max="2032-12-31"/><br/>
			</div>

			<div class="col-md-12">
				<input class="alert alert-success" type="submit" value="Cadastrar" />
			</div>

		</div>
	</form>


</div>

<?php include ('rodape.php')?>