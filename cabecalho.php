<!DOCTYPE html>
<html lang="pt-br">
<?php 

session_start();
if((!isset ($_SESSION['login'])) and (!isset ($_SESSION['senha'])))
{
  unset($_SESSION['login']);
  unset($_SESSION['senha']);
  header('location:index.php');
  }
 
$logado = $_SESSION['login'];
?>


<style type="text/css">
	body {
		background: url(imagens/bg-efeito.png) no-repeat center top;

	}
</style>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Sistema de Inclusão de GMUDs">
	<meta name="author" content="Leandro Ribeiro">
	<link rel="icon" href="../../../../favicon.ico">

	<title>IT Manager</title>

	<!-- JS dependencies -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<!-- bootbox code -->
	<script src="js/bootbox.min.js"></script>
	<!-- JS -->
	<script src="js/script.js"></script>
	

	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	<link href="css/styles.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<style type="text/css">
		body {
			padding-top: 5rem;
		}

		.starter-template {
			padding: 3rem 1.5rem;
			text-align: center;
		}

		#pacote {
			width: 300px;
		}
	</style>
</head>


<body>
	<main role="main" class="container">
		<nav class="navbar navbar-expand-md navbar-light bg-light fixed-top">
			<img class="navbar-brand" src="imagens/layout_set_logo.png"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

			<div class="collapse navbar-collapse">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="site.php">Home <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item active">
						<?php 
						if ($_SESSION['perm'] != 1){ ?>
							<a class="nav-link" href="cadastro.php">Cadastro</a>
						<?php } ?>
						
						
						
					</li>
					<li class="nav-item active">
						<a class="nav-link disabled" href="consulta-cadastro.php">Consulta</a>
					</li>
					<li class="nav-item active">
						<a class="nav-link disabled" href="dashboard.php">Dashboard</a>
					</li>
					<li class="nav-item active">
						<a class="nav-link disabled" href="painel-usuario.php">Painel Usuário</a>
					</li>
				</ul>

				<form class="form-inline my-2 my-lg-0">
				<a class="nav-link">Bem Vindo, <?php    
					  echo $_SESSION['nome'];
 				?></a>
				 <a hidden class="nav-link"><?php    
					  
					  echo $_SESSION['perm'];
 				?></a>
					<a class="btn btn-outline-danger my-2 my-sm-0" href="logout.php">Logout</a>
				</form>
			</div>
		</nav>