<?php
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    require 'PHPMailer/src/PHPMailer.php';
    require 'PHPMailer/src/Exception.php';
    require 'PHPMailer/src/SMTP.php';
    
    function enviaEmailnovaDemanda($nome,$status,$body,$emails){
    $Mailer = new PHPMailer();
    $Mailer->IsHTML(true);
    $Mailer->IsSMTP();
    $Mailer->SMTPDebug = false;
    //Aceita Caracteres Especiais
    $Mailer->Charset = 'UTF-8';

    //Configurações:
    $Mailer->SMTPAuth = true;
    $Mailer->SMTPSecure = 'TLS';

    $Mailer->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );

    //  Nome do Server:
    $Mailer->Host = 'smtp.office365.com';
    $Mailer->Port = 587;

    // Dados do usuário
    $Mailer->Username = 'librait.demandas@outlook.com';
    $Mailer->Password = '1424561424@1';

    $Mailer->From = 'librait.demandas@outlook.com';
    $Mailer->FromName = 'LibraIT';

    $Mailer->Subject = utf8_decode('Nova Demanda Cadastrada: '.$nome);
    $Mailer->Body = utf8_decode($body);
    $Mailer->AltBody = '';//Tava assim: $Mailer->AltBody = 'Conteudo body em texto'; não sei para o que ser, tirei.

    //Destinatario:
        
        foreach ($emails as $email) {
            //var_dump($email['email']);
            $Mailer->AddAddress($email['email']);

         }
    

   
    
    if($Mailer->Send()){

       return true;

    }

    return false;
}

function enviaEmailDemandaAtualizada($nome,$status,$body, $emails){
    $Mailer = new PHPMailer();
    $Mailer->IsHTML(true);
    $Mailer->IsSMTP();
    $Mailer->SMTPDebug = false;
    //Aceita Caracteres Especiais
    $Mailer->Charset = 'UTF-8';

    //Configurações:
    $Mailer->SMTPAuth = true;
    $Mailer->SMTPSecure = 'TLS';

    $Mailer->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );

    //  Nome do Server:
    $Mailer->Host = 'smtp.office365.com';
    $Mailer->Port = 587;

    // Dados do usuário Antigo
    //$Mailer->Username = 'librait.demandas@outlook.com';
    //$Mailer->Password = '1424561424@1';
    //$Mailer->From = 'librait.demandas@outlook.com';

    // Dados do usuário
    $Mailer->Username = 'itmanagerdemandas@outlook.com';
    $Mailer->Password = '1424561424@1';
    $Mailer->From = 'itmanagerdemandas@outlook.com';
    $Mailer->FromName = 'IT Manager';

    $Mailer->Subject = utf8_decode('Atualização status da demanda "'.$nome.'" - Status: '.$status);
    $Mailer->Body = utf8_decode($body);
    $Mailer->AltBody = '';//Tava assim: $Mailer->AltBody = 'Conteudo body em texto'; não sei para o que ser, tirei.

    //Destinatario:
        
        foreach ($emails as $email) {
            //var_dump($email['email']);
            $Mailer->AddAddress($email['email']);

         }
    

   
    
    if($Mailer->Send()){

       return true;

    }

    return false;
}

?>