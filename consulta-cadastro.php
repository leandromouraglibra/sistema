<?php include ('cabecalho.php');
include('conecta.php');
include('banco-demandas.php');

if(isset($_GET["nome"])){
$nome = $_GET["nome"];
$unidade = $_GET["unidade"];
$status = $_GET["status"];
$pacote = $_GET["pacote"];
$data_gmud = $_GET["data_gmud_ini"];
$data_gmud = $_GET["data_gmud_fim"];
$data_execucao = $_GET["data_execucao_ini"];
$data_execucao = $_GET["data_execucao_fim"];
$data_prd = $_GET["data_prd_ini"];
$data_prd = $_GET["data_prd_fim"];	
}
?>

<h1>Consulta Demandas</h1>
<form action="consulta-cadastro.php">
	<div class="row">
		<div class="col-md-6">
			Nome: <input class="form-control" type="text" name="nome" /><br/>
		</div>
		<div class="col">
			Unidade: 
				<select class="form-control" name="unidade">
			
				<option value=""></option>
				<option value="TEVAL">TEVAL</option>
				<option value="CAMPINAS">CAMPINAS</option>
				<option value="RIO">RIO</option>
				<option value="INTERMODAL">INTERMODAL</option>
			
				</select>
		</div>		
		<div class="col">
			Status:
			<select class="form-control" name="status">
			
				<option value=""></option>
				<option value="Desenvolvimento">Desenvolvimento</option>
				<option value="GMUD em Andamento">GMUD em Andamento</option>
				<option value="Homologação">Homologação</option>
				<option value="Produção">Produção</option>
			
				</select>
		</div>
		<div class="col-md-2">
			Pacote / Demanda: <input class="form-control" type="text" name="pacote" /><br/>
		</div>
		
		<div class="col-md-3">
			Data GMUD Inicio: <input class="form-control" type="date" name="data_gmud_ini" /><br/>
		</div>
		<div class="col-md-3">
			Data GMUD Fim: <input class="form-control" type="date" name="data_gmud_fim" /><br/>
		</div>
		<div class="col-md-3">
			Data Execução QA Inicio: <input class="form-control" type="date" name="data_execucao_ini" /><br/>
		</div>
		<div class="col-md-3">
			Data Execução QA Fim: <input class="form-control" type="date" name="data_execucao_fim" /><br/>
		</div>
		<div class="col-md-3">
			Data Execução PRD Inicio: <input class="form-control" type="date" name="data_prd_ini" /><br/>
		</div>
		<div class="col-md-3">
			Data Execução PRD Fim: <input class="form-control" type="date" name="data_prd_fim" /><br/>
		</div>
		<div class="col">
			
		<input class="btn btn-primary" type="submit" value="Consultar" style="margin-top:22px"  />
			</div>
	</div>



	<table class="table table-striped table-bordered">
		<th>ID</th>
		<th>Nome da Demanda</th>
		<th>Descrição</th>
		<th>Unidade</th>
		<th>Status</th>
		<th></th>
		<?php	
	$demandas = listaDemandas($conexao);
	foreach($demandas as $demanda) {
?>			
			<tr>
				<td>
					<?= $demanda['id'] 	?>
				</td>
				<td>
					<?= wordwrap($demanda['nome'],30,"<br />",true); 	?>
				</td>
				<td>
					<?= wordwrap($demanda['descricao'],40,"<br/>",true); 	?>
				</td>
				<td>
					<?= $demanda['unidade'] 	?>
				</td>
				<td>
					<?= $demanda['status'] 	?>
				</td>
				<td>
					<a href="detalhe-demanda.php?id=<?=$demanda['id']?>" class="text-success">Detalhes</a><br/>
				</td>
			</tr>

			<?php
	}
?>

	</table>