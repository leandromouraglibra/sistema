<?php include ('cabecalho.php');
include('conecta.php');
include('banco-demandas.php'); 
$id = $_GET["id"];
?>

<h1>Atualiza da Demanda</h1>
<form action="update-cadastro.php">
<div class="form-group">
	<?php	
			$demandas = detalheDemanda($conexao, $id);
			foreach($demandas as $demanda) {
?>

		<div class="row">
			
				<input class="form-control" type="hidden" required="" name="id_demanda" value="<?= $demanda['id'] ?>" /><br/>
			
			<div class="col">
				Nome: <input class="form-control" type="text" required="" name="nome" value="<?= $demanda['nome'] ?>"  /><br/>
			</div>
			<div class="cod-md-4">
				Unidades:
				<select class="form-control" name="unidade" required>
			
				<option value=""></option>
				<option value="TEVAL" <?php if($demanda['unidade'] == "TEVAL") echo "selected"; ?> >TEVAL</option>
				<option value="CAMPINAS" <?php if($demanda['unidade'] == "CAMPINAS") echo "selected"; ?> >CAMPINAS</option>
				<option value="RIO"  <?php if($demanda['unidade'] == "RIO") echo "selected"; ?> >RIO</option>
				<option value="INTERMODAL" <?php if($demanda['unidade'] =="INTERMODAL") echo "selected"; ?> >INTERMODAL</option>
			
				</select>
			</div>
			<div class="col">
				Pacote: <input class="form-control" type="text" name="pacote" value="<?= $demanda['pacote'] ?> " /><br/>
			</div>

			<div class="col-md-12">
				Descrição: <textarea class="form-control" type="text" required="" name="descricao" ><?= $demanda['descricao'] ?></textarea><br/>
			</div>
			<div class="col">
				Data GMUD: <input class="form-control" type="date" name="data_gmud" value="<?= $demanda['data_gmud'] ?>" /><br/>
			</div>
			<div class="col">
				Data Execução QA: <input class="form-control" type="date" name="data_execucao" value="<?= $demanda['data_execucao'] ?>" /><br/>
			</div>
			<div class="col">
				Data Execução PRD: <input class="form-control" type="date" name="data_prd" value="<?= $demanda['data_prd'] ?>" /><br/>
			</div>
			<div class="col">
					<input class="btn btn-primary" type="submit" value="Atualizar" style="margin-top:22px" />
				</div>
			
			



		</div>
	
	
			<?php
	}
?>