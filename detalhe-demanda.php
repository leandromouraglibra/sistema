<?php include ('cabecalho.php');
include('conecta.php');
include('banco-demandas.php'); 
$id = $_GET["id"];

?>

<h1>Detalhes da Demanda</h1>
<div class="form-group">
	<?php	
			$demandas = detalheDemanda($conexao, $id);
			$demanda = $demandas[0];
	?>

		<div class="row">
			<div class="col-md-2">
				ID: <input class="form-control" type="text" required="" name="id_demanda" value="<?= $demanda['id'] ?>" disabled/><br/>
			</div>
			<div class="col">
				Nome: <input class="form-control" type="text" required="" name="nome" value="<?= $demanda['nome'] ?>" disabled /><br/>
			</div>
			<div class="cod-md-4" >
				Unidades:
				<select class="form-control" name="unidade" required disabled>
			
				<option value=""></option>
				<option value="TEVAL" <?php if($demanda['unidade'] == "TEVAL") echo "selected"; ?> >TEVAL</option>
				<option value="CAMPINAS" <?php if($demanda['unidade'] == "CAMPINAS") echo "selected"; ?> >CAMPINAS</option>
				<option value="RIO"  <?php if($demanda['unidade'] == "RIO") echo "selected"; ?> >RIO</option>
				<option value="INTERMODAL" <?php if($demanda['unidade'] =="INTERMODAL") echo "selected"; ?> >INTERMODAL</option>
			
				</select>
			</div>
			<div class="col">
				Pacote: <input class="form-control" type="text" name="pacote" value="<?= $demanda['pacote'] ?> " disabled/><br/>
			</div>

			<div class="col-md-12">
				Descrição: <textarea class="form-control" type="text" required="" name="descricao" disabled><?= $demanda['descricao'] ?></textarea><br/>
			</div>
			<div class="col">
				Data GMUD: <input class="form-control" type="date" name="data_gmud" value="<?= $demanda['data_gmud'] ?>" disabled/><br/>
			</div>
			<div class="col">
				Data Execução QA: <input class="form-control" type="date" name="data_execucao" value="<?= $demanda['data_execucao'] ?>" disabled/><br/>
			</div>
			<div class="col">
				Data Execução PRD: <input class="form-control" type="date" name="data_prd" value="<?= $demanda['data_prd'] ?>" disabled/><br/>
			</div>
			<div class="col">
				Status: <input class="form-control" type="text" name="status" value="<?= $demanda['status'] ?>" disabled/><br/>
			</div>
			<div class="col-md-6">
			<?php 
						if ($_SESSION['perm'] != 1){ ?>
			<input class="btn btn-primary" type="button" value="Alterar Demanda" onclick="window.location.href='atualiza-cadastro.php?id=<?=$id?>'"/></button>
			<a class="btn btn-primary" href="mailto:?subject= <?= $demanda['nome'] ?>&body=Prezado,%0A
			%0A
			Poderia, por gentileza, dar o de acordo para execução da demanda conforme abaixo?%0A
			%0A
			%09Título: <?= $demanda['nome'] ?> %0A
			%09Ambiente:<?php 
						if($demanda['status'] != "Em homologação"){
							echo " Homologação";	}
						else {
							echo " Produção";
						} ?> %0A
			%09Equipes: %0A
			%09Líder Técnico: <?php echo $_SESSION['nome'];	?>%0A
			%09Data: %0A
			%09Código Planilha de Controle: %0A
			">Solicitar Aprovação</a>
			<a href="#" class="btn btn-danger" type="button" value="Excluir Demanda" onclick="confirmExcluiDemanda(<?=$id?>)" >Excluir Demanda</a>
			<?php } ?>
			</div>
			
																			


		</div>

		<?php 
						if ($_SESSION['perm'] != 1){ ?>
		<h1>Itens da Demanda</h1>
		<form action="adiciona-detalhe.php">
			<div class="row">
				<INPUT TYPE="hidden" NAME="id_demanda" VALUE="<?= $demanda['id'] ?>">
				<div class="col">
					TSC²: <input class="form-control" type="text" name="chamado_tsc2" /><br/>
				</div>
				<div class="col">
					Fornecedor: <input class="form-control" type="text" name="chamado_evolua" required/><br/>
				</div>
				<div class="col">
					Tipos Arquivos: <input class="form-control" type="text" required="" name="tip_arquivo" /><br/>
				</div>
				<div class="col">
					Data Inicio Dev: <input class="form-control" type="date" name="data_inicio" min="2018-01-01" max="2032-12-31" required /><br/>
				</div>									
				<div class="col">
					Data Conclusão Dev: <input class="form-control" type="date" name="data_conclusao" min="2018-01-01" max="2032-12-31" required/><br/>
				</div>
				<div class="col-md-10">
					Descrição: <textarea class="form-control" type="text" name="descricao" required /></textarea><br/>
				</div>
				
				<div class="col-md-2">
					<input class="btn btn-primary" type="submit" value="Cadastrar" style="margin-top:22px" />
					<?php } ?>
				</div>
				




				<table class="table table-striped table-bordered">
					<th>Item</th>
		<th>Chamado TSC²</th>
		<th>Chamado Fornecedor</th>
		<th>Tipo Arquivo</th>
		<th>Data Inicio Dev</th>
		<th>Data Conclusão Dev</th>
		<th>Descrição</th>
		<?php 
							if ($_SESSION['perm'] != 1){ ?>
		<th></th>
		<?php } ?>

					<?php	
				$demandaItens = listaDemandasItens($conexao, $id);
				$i = 1;			   
					   
				foreach($demandaItens as $demandaItem) {
?>
						<tr>
							<td>
								<?= $i++;	?>
							</td>							
							<td>
								<?= $demandaItem['chamado_tsc2'] 	?>
							</td>
							<td>
								<?= $demandaItem['chamado_evolua'] 	?>
							</td>
							<td>
								<?= $demandaItem['tip_arquivo'] 	?>
							</td>
							<td>
								<?= $demandaItem['data_inicio'] 	?>
							</td>
							<td>
								<?= $demandaItem['data_conclusao'] 	?>
							</td>
							<td>
								<?= $demandaItem['descricao'] 	?>
							</td>
							<?php 
							if ($_SESSION['perm'] != 1){ ?>
								<td>
								
									<a href="#" onclick="confirmExcluiItemDemanda(<?=$demandaItem['id']?>,<?=$demanda['id']?>)" class="text-danger"> Remover</a>
								</td>
							<?php } ?>
						</tr>


						<?php
						}
					?>

				</table>


				<?php include ('rodape.php')?>