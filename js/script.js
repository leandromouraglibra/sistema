function confirmExcluiDemanda(id) {			
    bootbox.confirm("Tem certeza que deseja excluir a demanda?", function(result){ 				 
        if (result){
	    console.log('This was logged in the callback: ' + id);
	    document.location.href = 'remove-demanda.php?id='+id;
        }
    });            
}

function confirmExcluiItemDemanda(id, idDemanda) {			
    bootbox.confirm("Tem certeza que deseja excluir o item da demanda?", function(result){ 				 
        if (result){
        console.log('This was logged in the callback: ' + id);
        document.location.href = 'remove-item.php?id='+id+'&d='+idDemanda;
        
        }
    });            
}

function verificaRetornoItem(){
    var url = new URL(document.location.href);
    var r = url.searchParams.get("r");
    if (r == undefined){
        return;
    }
    if (r == 1 ){
        bootbox.alert("Item excluído com sucesso");
    }
    if (r == "R0dr1g0" || r == "M4rc10"){
        bootbox.alert("<3");
    }
}

$(document).ready(function(){
    verificaRetornoItem();
});